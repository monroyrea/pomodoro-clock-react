import React from 'react';
import TimeControl from '../pomodoroClockApp/TimeControl';
import Display from '../pomodoroClockApp/Display';
import '../pomodoroClockApp/pomodoro-clock-container.css';

class PomodoroClockContainer extends React.Component{
    constructor(props){
        super(props)
        this.state = {
            breakTime : 5,
            sessionTime : 25,
            displayMinutes : 25,
            displaySeconds : 0,
            isSession : true,
            isPlaying : false,
            isReset : false
        }
        this.changeBreakTime = this.changeBreakTime.bind(this);
        this.changeSessionTime = this.changeSessionTime.bind(this);
        this.play = this.play.bind(this);
        this.reset = this.reset.bind(this);
        this.tick = this.tick.bind(this);
        this.timeout = null;
    };

    render(){
        return(
            <div id="pomodoro-container">
                <header className="pomodoro-header">
                    Pomodoro Clock
                </header>
                <div className="pomodoro-controls">                
                    <TimeControl id="break" label="Break Length" disabled={this.state.isPlaying} value={this.state.breakTime} changeTime={this.changeBreakTime} ></TimeControl>
                    <TimeControl id="session" label="Sesssion Length" disabled={this.state.isPlaying} value={this.state.sessionTime} changeTime={this.changeSessionTime}></TimeControl>  
                </div>
                <div className="pomodoro-display">
                <Display type={this.state.isSession} displayMinutes={this.state.displayMinutes} displaySeconds={this.state.displaySeconds}></Display>
                </div>
                <div className="pomodoro-buttons">
                    <button id="start_stop" onClick={this.play}><i className="far fa-play-circle fa-3x"></i></button>
                    <button onClick={this.stop}><i className="far fa-stop-circle fa-3x"></i></button>
                    <button id="reset" onClick={this.reset} ><i className="fas fa-sync fa-3x"></i></button>
                </div> 
            </div>
        )
    }

    render(){
        return(
            <div id="pomodoro-container">
                <header className="pomodoro-header">
                    Pomodoro Clock
                </header>
                <div className="pomodoro-controls">                
                    <TimeControl id="break" label="Break Length" disabled={this.state.isPlaying} value={this.state.breakTime} changeTime={this.changeBreakTime} ></TimeControl>
                    <TimeControl id="session" label="Sesssion Length" disabled={this.state.isPlaying} value={this.state.sessionTime} changeTime={this.changeSessionTime}></TimeControl>  
                </div>
                <div className="pomodoro-display">
                <Display type={this.state.isSession} displayMinutes={this.state.displayMinutes} displaySeconds={this.state.displaySeconds} reset={this.state.isReset}></Display>
                </div>
                <div className="pomodoro-buttons">
                    <button id="start_stop" onClick={this.play}><i className="far fa-play-circle fa-3x"></i><i className="far fa-stop-circle fa-3x"></i></button>
                    <button id="reset" onClick={this.reset} ><i className="fas fa-sync fa-3x"></i></button>
                </div> 
            </div>
        )
    }

    changeBreakTime = (value) =>{
        this.setState(() => {
            return Object.assign({}, this.state, {breakTime : value, isReset:false});
        })
    };

    changeSessionTime = (value) =>{
        this.setState(() => {
            return Object.assign({}, this.state, {sessionTime : value, displayMinutes: value, displaySeconds: 0, isReset:false });
        })
    };

    play = ()=>{
        if(!this.state.isPlaying){
            this.setState(() => {
                return Object.assign({}, this.state, {isPlaying : true, isReset : false});
            })
            this.timeout = setInterval(this.tick, 1000);
        }else{
            this.setState(() => {
                return Object.assign({}, this.state, {isPlaying : false, isReset : false});
            })
            clearInterval(this.timeout);
        }
    };

    tick(){
        let minutes = this.state.displayMinutes;
        let seconds = this.state.displaySeconds;

        if(seconds === 0){
            console.log(minutes + " " + seconds)
            minutes = minutes -1;
            seconds = 59;
        }else{
            console.log(minutes + " " + seconds)
            seconds = seconds -1;
        }

        if(minutes === -1 && seconds === 59){
            if(this.state.isSession){
                this.setState(() => {
                    return Object.assign({}, this.state, {isSession:false, displayMinutes : this.state.breakTime, displaySeconds: 0});
                })
            }else{
                this.setState(() => {
                    return Object.assign({}, this.state, {isSession:true, displayMinutes : this.state.sessionTime, displaySeconds: 0});
                })
            }
        }else{
            this.setState(() => {
                return Object.assign({}, this.state, {displayMinutes : minutes, displaySeconds: seconds});
            })
        }
    }

    reset = ()=>{
        this.setState(() => {
            return Object.assign({}, this.state, 
                {
                    breakTime : 5,
                    sessionTime : 25,
                    displayMinutes: 25,
                    displaySeconds: 0,
                    isSession : true,
                    isPlaying : false,
                    isReset : true
                });
        })
        clearInterval(this.timeout);
    };
}

export default PomodoroClockContainer;