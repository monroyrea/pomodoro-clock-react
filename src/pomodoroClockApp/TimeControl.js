import React from 'react';
import PropTypes from 'prop-types';
import '../pomodoroClockApp/TimeControl.css';


class TimeControl extends React.Component{
    constructor(props){
        super(props);
        this.increment = this.increment.bind(this);
        this.decrease = this.decrease.bind(this);
    };

    increment = () =>{
        let value = this.props.value + 1;
        if(value >60)
            value = 60;
        this.props.changeTime(value);
    }

    decrease = () => {
        let value = this.props.value -1;
        if(value <1)
            value = 1;
        this.props.changeTime(value);
    }

    render(){
        return(
            <div className="timer-control">
                <div className="timer-header">
                    <header id={this.props.id + "-label"}>{this.props.label}</header>
                </div>
                <div className="timer-buttons">
                    <button id={this.props.id + "-increment"} onClick={this.increment} disabled={this.props.disabled}>
                        <i className="far fa-arrow-alt-circle-up fa-3x"></i>
                    </button>
                    <span id={this.props.id + "-length"}>{this.props.value}</span>
                    <button id={this.props.id + "-decrement"} onClick={this.decrease} disabled={this.props.disabled}>
                        <i className="far fa-arrow-alt-circle-down fa-3x"></i>
                    </button>
                </div>
            </div>

        )
    }
}

TimeControl.propTypes = {
    id: PropTypes.string.isRequired,
    label : PropTypes.string.isRequired,
    disabled : PropTypes.bool.isRequired,
    value : PropTypes.number.isRequired,
    changeTime : PropTypes.func.isRequired
}

export default TimeControl;