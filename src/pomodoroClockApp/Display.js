import React from 'react';
import PropTypes from 'prop-types';
import './display.css';

class Display extends React.Component{
    constructor(props){
        super(props);
    this.minutes = null;
    this.seconds = null;
    this.audioRef = React.createRef(); 
    }

    componentWillMount(){
        this.minutes = this.props.displayMinutes < 10 ? "0" + this.props.displayMinutes : this.props.displayMinutes;
        this.seconds = this.props.displaySeconds < 10 ? "0" + this.props.displaySeconds : this.props.displaySeconds;
    }

    componentWillUpdate(nextProps){
        if(nextProps.reset){
            this.audioRef.current.pause();
            this.audioRef.current.currentTime = 0;
        }
        this.minutes = nextProps.displayMinutes < 10 ? "0" + nextProps.displayMinutes : nextProps.displayMinutes;
        this.seconds = nextProps.displaySeconds < 10 ? "0" + nextProps.displaySeconds : nextProps.displaySeconds;
        if(this.props.type && nextProps.displayMinutes ===0 && nextProps.displaySeconds ===1)
            this.audioRef.current.play();

    }

    render(){
        return (
            <div className="timer">
                <p id="timer-label" className="timer-label">
                    {this.props.type ? "Session" : "Break" }
                </p>
                <audio id="beep" ref={this.audioRef} src="https://goo.gl/65cBl1"></audio>
                <p id="time-left" className="time-left">
                    {this.minutes + ":" + this.seconds}
                </p>
            </div>
        )
    }
}

Display.propTypes = {
    displayMinutes : PropTypes.number.isRequired,
    displaySeconds : PropTypes.number.isRequired,
    type : PropTypes.bool.isRequired
}   

export default Display;
