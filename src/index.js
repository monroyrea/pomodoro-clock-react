import React from 'react';
import ReactDOM from 'react-dom';
import PomodoroClockContainer from "./pomodoroClockApp/PomodoroClockContainer"
import './index.css';
import registerServiceWorker from './registerServiceWorker';

ReactDOM.render(<PomodoroClockContainer />, document.getElementById('root'));
registerServiceWorker();
